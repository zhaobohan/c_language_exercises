#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#define N 5

int main()
{
	int a[N];
	int i, j;
	for (i = 0; i < N; i++)
	{
		printf("Number %d :", i + 1);
		scanf("%d", &a[i]);
	}
	int op[N][N];
	int e;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			op[i][j] = a[i] * a[j];
		}
	}
	int  flag = 1;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			for (int k = 0; k < N; k++)
			{
				if (op[i][j] * a[k] != a[i] * op[j][k])
				{
					printf("运算是不可结合\n");
					flag = 0;
				}
			}
		}
	}
	if (flag)
	{
		printf("运算是可结合\n");
	}
	flag = 0;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			if (op[i][j] != a[j] || op[j][i] != a[j])
			{
				break;
			}
		}
		if (j == N)
		{
			printf("群有幺元%d\n", a[i]);
			e = a[i];
			flag = 1;
			break;
		}
	}
	if (!flag)
	{
		printf("群没有幺元\n");
	}
	flag = 1;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			if (op[i][j] == e && op[j][i] == e)
			{
				break;
			}
		}
		if (j == N - 1)
		{
			flag = 0;
			printf("A中元素%d没有逆元\n", a[j]);
		}
	}
	if (flag)
	{
		printf("A中任何元素都有逆元\n");
	}
}