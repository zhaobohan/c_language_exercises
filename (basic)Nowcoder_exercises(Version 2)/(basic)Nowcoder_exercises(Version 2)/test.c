﻿#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#define N 13
struct tree
{
    float num;
    struct tree* Lnode;
    struct tree* Rnode;
}*fp[N]; //保存结点

char s[2 * N]; //放前缀码

void inite_node(float f[], int n)
{ //生成叶子结点
    int i;
    struct tree* pt;
    for (i = 0; i < n; i++)
    {
        pt = (struct tree*)malloc(sizeof(struct tree));//生成叶子结点
        pt->num = f[i];
        pt->Lnode = NULL;
        pt->Rnode = NULL;
        fp[i] = pt;
    }
}

void sort(struct tree* array[], int n)
{ //将第N-n个点插入到已排好序的序列中。
    int i;
    struct tree* temp;
    for (i = N - n; i < N - 1; i++)
    {
        if (array[i]->num > array[i + 1]->num)
        {
            temp = array[i + 1];
            array[i + 1] = array[i];
            array[i] = temp;
        }
    }
}

struct tree* construct_tree(float f[], int n)
{ //建立树
    int i;
    struct tree* pt;
    for (i = 1; i < N; i++)
    {
        pt = (struct tree*)malloc(sizeof(struct tree));//生成非叶子结点
        pt->num = fp[i - 1]->num + fp[i]->num;
        pt->Lnode = fp[i - 1];
        pt->Rnode = fp[i];
        fp[i] = pt;//w1+w2
        sort(fp, N - i);
    }
    return fp[N - 1];
}

void preorder(struct tree* p, int k, char c)
{
    int j;
    if (p != NULL)
    {
        if (c == 'l')
            s[k] = '0';
        else
            s[k] = '1';
        if (p->Lnode == NULL)
        {
            //P指向叶子
            printf("%.2f:  ", p->num);
            for (j = 0; j <= k; j++)
                printf("%c", s[j]);
            putchar('\n');
        }
        preorder(p->Lnode, k + 1, 'l');
        preorder(p->Rnode, k + 1, 'r');
    }
}

int main()
{
    float f[N] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41 };
    struct tree* head;
    inite_node(f, N); //初始化结点
    head = construct_tree(f, N);//生成最优树
    s[0] = 0;
    preorder(head, 0, 'l');//遍历树
    return 0;
}



//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
//
//int n, a[100][100], m = 0, visit[100];
//int cur = 0, s[100], vis[100][100];
//
//void try1(int k)
//{
//    int i;
//    if (cur == m + 1)
//    {
//        for (i = 0; i < cur; i++)
//        {
//            if (i == 0)
//                printf("%d", s[i] + 1);
//            else
//                printf("->%d", s[i] + 1);
//        }
//        printf("\n");
//    }
//    else
//    {
//        for (i = 0; i < n; i++)
//        {
//            if (a[k][i] && !vis[k][i])
//            {
//                vis[k][i] = 1;
//                vis[i][k] = 1;
//                s[cur++] = i;
//                try1(i);
//                cur--;
//                vis[k][i] = 0;
//                vis[i][k] = 0;
//            }
//        }
//    }
//}
//
//void dfs(int k)
//{
//    int i;
//    visit[k] = 1;
//    for (i = 0; i < n; i++)
//    {
//        if (a[k][i] && !visit[i])
//        {
//            dfs(i);
//        }
//    }
//}
//
//int fun()
//{
//    int i;
//    dfs(0);
//    for (i = 0; i < n; i++)
//    {
//        if (!visit[i])
//            return 0;
//    }
//    return 1;
//}
//
//int main()
//{
//    int i, j;
//    printf("请输入节点个数n：");
//    scanf("%d", &n);
//    for (i = 0; i < n; i++)
//        for (j = 0; j < n; j++)
//            scanf("%d", &a[i][j]);
//    printf("邻接矩阵为：\n");
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n; j++)
//            printf("%d ", a[i][j]);
//        printf("\n");
//    }
//    if (fun() == 0)
//    {
//        printf("该图不是连通图\n");
//        return 0;
//    }
//    printf("该图是连通图\n");
//    int odd = 0;
//    for (i = 0; i < n; i++)
//    {
//        int sum = 0;
//        for (j = 0; j < n; j++)
//        {
//            if (a[i][j])
//                sum++;
//        }
//        if (sum % 2)
//            odd++;
//    }
//
//    if (odd == 0)
//    {
//        printf("该图是欧拉图。\n");
//        printf("所有的欧拉路为：\n");
//        for (i = 0; i < n; i++)
//        {
//            s[cur++] = i;
//            try1(i);
//            cur--;
//        }
//    }
//    else
//        printf("不是欧拉图\n");
//    return 0;
//}




//#include <stdio.h>
//int main()
//{
//	int a, max = 0, small = 100, sum = 0, count = 0;
//	while (scanf("%d", &a) != EOF)
//	{
//		if (a > max)//判定最高分
//		{
//			max = a;
//		}
//		if (a < small)//判定最低分
//		{
//			small = a;
//		}
//		sum += a;
//		count++;//计数器
//		if (count == 7)//计数器=7时代表一组的分数好了可以进行计算
//		{
//			printf("%.2f\n", (sum - max - small) / 5.0);
//			count = 0;//重置
//			max = 0;//重置
//			small = 100;//重置
//			sum = 0;//重置
//		}
//
//	}
//
//	return 0;
//}


//BC46
//描述
//KiKi开始学习英文字母，BoBo老师告诉他，有五个字母A(a), E(e), I(i), O(o), U(u)称为元音，其他所有字母称为辅音，请帮他编写程序判断输入的字母是元音（Vowel）还是辅音（Consonant）。
//输入描述：
//多组输入，每行输入一个字母。
//输出描述：
//针对每组输入，输出为一行，如果输入字母是元音（包括大小写），输出“Vowel”，如果输入字母是非元音，输出“Consonant”。
//
//#include <stdio.h>
//
//int main()
//{
//    char s;
//    while (scanf("%c", &s) != EOF)
//    {
//        if (s >= 'a' && s <= 'z')
//            s = s - 32;
//        if (s == 'A' || s == 'E' || s == 'I' || s == 'O' || s == 'U')
//            printf("Vowel\n");
//        else
//            printf("Consonant\n");
//        getchar();
//    }
//    return 0;
//}

//BC45
//描述
//KiKi参加了语文、数学、外语的考试，请帮他判断三科中的最高分。从键盘任意输入三个整数表示的分数，编程判断其中的最高分。
//
//数据范围：
//0
//≤
//�
//≤
//100
//
//0≤n≤100
//输入描述：
//输入一行包括三个整数表示的分数（0~100），用空格分隔。
//输出描述：
//输出为一行，即三个分数中的最高分。
//#include <stdio.h>
//
//int main()
//{
//    int a, b, c;
//    scanf("%d %d %d", &a, &b, &c);
//    if (b > a)
//        a = b;
//    if (c > a)
//        a = c;
//    printf("%d", a);
//    return 0;
//}

//BC44
//描述
//KiKi想知道一个整数的奇偶性，请帮他判断。从键盘任意输入一个整数（范围 - 231~231 - 1），编程判断它的奇偶性。
//输入描述：
//多组输入，每行输入包括一个整数。
//输出描述：
//针对每行输入，输出该数是奇数（Odd）还是偶数（Even）。
//#include <stdio.h>
//
//int main() {
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        if (a % 2 == 0)
//            printf("Even\n");
//        else
//            printf("Odd\n");
//    }
//    return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//    int m, n, arr[11][11];
//    scanf("%d %d", &m, &n);
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    for (int i = 0; i < n; i++)//3
//    {
//        for (int j = 0; j < m; j++)//2
//        {
//            printf("%d ", arr[j][i]);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n, arr[11][11];
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    for (int i = 1; i < n; i++)
//    {
//        for (int j = 0; j < i; j++)
//        {
//            if (arr[i][j] != 0)
//            {
//                printf("NO\n");
//                return 0;
//            }
//        }
//    }
//    printf("YES\n");
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int m, n, arr1[11][11], arr2[11][11], flag = 0;
//    scanf("%d %d", &m, &n);
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr1[i][j]);
//        }
//    }
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr2[i][j]);
//            if (arr1[i][j] != arr2[i][j])
//            {
//                flag = 1;
//                break;
//            }
//        }
//        if (flag == 1)
//            break;
//    }
//    if (flag == 0)
//        printf("Yes\n");
//    else
//        printf("No\n");
//    return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//    int m, n, arr[11][11], max = -1;
//    scanf("%d %d", &m, &n);
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr[i][j]);
//            if (arr[i][j] > max)
//                max = arr[i][j];
//        }
//    }
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            if (arr[i][j] == max)
//            {
//                printf("%d %d", i + 1, j + 1);
//            }
//        }
//    }
//    return 0;
//}


//#include <stdio.h>
//
//int main() {
//    int n, m, arr[11][11];
//    scanf("%d %d", &n, &m);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            printf("%d ", arr[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//#include <stdio.h>

//int main()
//{
//    int m, n, arr1[101], arr2[101];
//    scanf("%d %d", &m, &n);
//    for (int i = 0; i < m; i++)
//        scanf("%d", &arr1[i]);
//    for (int i = 0; i < n; i++)
//        scanf("%d", &arr2[i]);
//    int p = 0, q = 0;
//    while (p < m && q < n)
//    {
//        if (arr1[p] < arr2[q])
//        {
//            printf("%d ", arr1[p]);
//            p++;
//        }
//        else
//        {
//            printf("%d ", arr2[q]);
//            q++;
//        }
//    }
//    if (p < m)
//    {
//        for (; p < m; p++)
//            printf("%d ", arr1[p]);
//    }
//    else
//    {
//        for (; q < n; q++)
//            printf("%d ", arr2[q]);
//    }
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n, arr[1001], out[101], cout = 0, m = 0;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//        scanf("%d", &arr[i]);
//    out[0] = arr[0];
//    m++;
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < i; j++)
//        {
//            if (arr[i] != arr[j])
//            {
//                out[m] = arr[i];
//                m++;
//                break;
//            }
//        }
//    }
//    for (int i = 0; i <= m; i++)
//        printf("%d ", out[i]);
//    return 0;
//}

//#include <stdio.h>
//int arr[101];
//
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    int cout = 0;
//    for (int i = 0; i < n; i++)
//        scanf("%d", &arr[i]);
//    for (int i = 0; i < n-cout; i++)
//    {
//        for (int j = i + 1; j < n-cout; j++)
//        {
//            while (arr[i] == arr[j])
//            {
//                for (int m = j; m < n - cout - 1; m++)
//                arr[m] = arr[m + 1];
//                arr[n - 1 - cout] = -9999;
//                cout++;
//            }
//        }
//    }
//    for (int i = 0; i < n - cout; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    int arr[1001], cout = 0;
//    for (int i = 0; i < n; i++)
//         scanf("%d", &arr[i]);
//    for (int i = 0; i < n-cout; i++)
//    {
//        for (int j = i + 1; j < n-cout;)
//        {
//            if (arr[i] == arr[j])
//            {
//                for (int m = j; m < n - cout - 1; m++)
//                {
//                    arr[m] = arr[m + 1];
//                }
//                cout++;
//            }
//            if (arr[i] != arr[i + 1])
//                j++;
//        }
//    }
//    for (int i = 0; i < n - cout; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int n, arr[51];
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//        scanf("%d", &arr[i]);
//    int del;
//    int cout = 0;
//    scanf("%d", &del);
//    for (int i = n - 1; i >= 0; i--)
//    {
//
//        if (arr[i] == del)
//        {
//            for (int j = i;j<n-cout-1;j++)
//            {
//                arr[j] = arr[j+1];
//            }
//            cout++;
//        }
//    }
//    for (int i = 0; i < n - cout; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}
//#include <stdio.h>

//int main()
//{
//    int n, arr[51];
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//        scanf("%d", &arr[i]);
//    int del;
//    scanf("%d", &del);
//    int ret = 0;
//    while (n >= 0)
//    {
//        if (arr[n] == del)
//            ret = n;
//        n--;
//    }
//    for (int i = ret; i < n; i++)
//    {
//        arr[i] = arr[i + 1];
//    }
//    for (int i = 0; i < n - 1; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int a, arr[51];
//    scanf("%d", &a);
//    for (int i = 0; i < a; i++)
//        scanf("%d", &arr[i]);
//    int ins;
//    scanf("%d", &ins);
//    for (int i = a - 1; i >= 0; i--)
//    {
//        if (ins < arr[i])
//        {
//            arr[i + 1] = arr[i];
//            arr[i] = ins;
//        }
//        else if (ins > arr[a - 1])
//        {
//            arr[a] = ins;
//            break;
//        }
//    }
//    for (int i = 0; i < a + 1; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}

//BC96
//描述
//输入一个整数序列，判断是否是有序序列，有序，指序列中的整数从小到大排序或者从大到小排序(相同元素也视为有序)。
//
//数据范围：
//1≤val≤100
//输入描述：
//第一行输入一个整数N(3≤N≤50)。
//第二行输入N个整数，用空格分隔N个整数。
//输出描述：
//输出为一行，如果序列有序输出sorted，否则输出unsorted。	
//
//#include <stdio.h>
//
//int main() {
//    int a, arr[50], count = 0;
//    scanf("%d", &a);
//    for (int i = 0; i < a; i++)
//        scanf("%d", &arr[i]);
//    for (int i = 0; i < a - 1; i++)
//    {
//        if (arr[i] > arr[i + 1])
//            count++;
//        else
//            count--;
//    }
//    if (count == a - 1 || count == -a + 1)
//        printf("sorted\n");
//    else
//        printf("unsorted\n");
//    return 0;
//}

//BC39
//描述
//期中考试开始了，大家都想取得好成绩，争夺前五名。从键盘输入 n 个学生成绩，输出每组排在前五高的成绩。
//5≤n≤50  ，成绩采取百分制并不会出现负数
//输入描述：
//两行，第一行输入一个整数，表示n个学生（ >= 5），第二行输入n个学生成绩（整数表示，范围0~100），用空格分隔。
//输出描述：
//一行，输出成绩最高的前五个，用空格分隔。
//#include<stdio.h>
//int main() {
//    int arr[40] = { 0 };
//    int num = 0;
//    scanf("%d", &num);//输入几名
//    for (int k = 0; k <= num; k++) {//输入成绩
//        scanf("%d", &arr[k]);
//    }
//    int temp = 0;
//    for (int i = 0; i < num; i++) {//第i位为最大数
//        for (int j = i + 1; j < num; j++) {//从第i+1位开始遍历剩余数
//            if (arr[j] > arr[i]) {//存在大于最大数的数
//                temp = arr[i];//把最大数和比较数进行交换
//                arr[i] = arr[j];
//                arr[j] = temp;
//            }
//
//        }
//    }
//    for (int k = 0; k < 5; k++)
//        printf("%d ", arr[k]);
//
//    return 0;
//}

//BC38
//描述
//变种水仙花数 - Lily Number：把任意的数字，从中间拆分成两个数字，比如1461 可以拆分成（1和461）, （14和61）, （146和1), 如果所有拆分后的乘积之和等于自身，则是一个Lily Number。
//
//例如：
//
//655 = 6 * 55 + 65 * 5
//
//1461 = 1 * 461 + 14 * 61 + 146 * 1
//
//求出 5位数中的所有 Lily Number。
//
//输入描述：
//无
//输出描述：
//一行，5位数中的所有 Lily Number，每两个数之间间隔一个空格。
//#include <stdio.h>
//int main()
//{
//    for (int i = 10000; i <= 99999; i++)
//    {
//        if (((i/10000)*(i%10000)+(i / 1000) * (i % 1000) + (i / 100) * (i % 100) + (i / 10) * (i % 10) ) == i)
//            printf("%d ", i);
//    }
//    printf("\b");
//    return 0;
//}
//#include &amp;lt;iostream&amp;gt;
//
//using namespace std;
//
//int main()
//{
//    float a[5][5];//五个学生，没人五科成绩，所以定义数组为五行五列
//    for (int i = 0; i & amp; lt; 5; i++)
//    {
//        float sum = 0;
//        for (int j = 0; j & amp; lt; 5; j++)
//        {
//            cin& amp; gt; &amp; gt; a[i][j];//输入每个人的每科成绩
//            sum += a[i][j];//总分
//            cout& amp; lt; &amp; lt; a[i][j] & amp; lt; &amp; lt; &amp; quot; &amp; quot;;
//        }
//        cout& amp; lt; &amp; lt; sum& amp; lt; &amp; lt; endl;//输出总分
//    }
//    return 0;
//}
//
//
//#include<bits/stdc++.h>
//using namespace std;
//int main()
//{
//	int n;
//	cin >> n;
//	int a[n + 1];
//	int b[n + 1];
//	for (int i = 1; i <= n; i++)
//		cin >> a[i];
//	int k;
//	cin >> k;
//	int flag1 = 0, c1 = 0, c2 = 0;
//	for (int i = 1; i <= k; i++)
//	{
//		for (int j = 1; j <= n; j++)
//		{
//			cin >> b[j];
//			if (b[j] == 0)
//				c1++;
//		}
//		if (c1 == n)
//		{
//			printf("Ai Ya\n");
//			c1 = 0;
//		}
//		else
//		{
//			c1 = 0;
//			for (int j = 1; j <= n; j++)
//			{
//				if (b[j] != 0)
//				{
//					if (b[j] != a[j])
//					{
//						printf("Ai Ya\n");
//						flag1 = 1;
//						break;
//					}
//					else
//					{
//						c2++;
//					}
//				}
//			}
//			if (!flag1)
//				if (c2 >= 1)
//				{
//					printf("Da Jiang!!!\n");
//				}
//			flag1 = 0;
//			c2 = 0;
//		}
//
//	}
//}
//#include<bits/stdc++.h>
//using namespace std;
//int main()
//{
//	int n;
//	cin >> n;
//	int a[n + 1];
//	int b[n + 1];
//	for (int i = 1; i <= n; i++)
//		cin >> a[i];
//	int k;
//	cin >> k;
//	int flag1 = 0, c1 = 0, c2 = 0;
//	for (int i = 1; i <= k; i++)
//	{
//		for (int j = 1; j <= n; j++)
//		{
//			cin >> b[j];
//			if (b[j] == 0)
//				c1++;
//		}
//		if (c1 == n)
//		{
//			printf("Ai Ya\n");
//			c1 = 0;
//		}
//		else
//		{
//			c1 = 0;
//			for (int j = 1; j <= n; j++)
//			{
//				if (b[j] != 0)
//				{
//					if (b[j] != a[j])
//					{
//						printf("Ai Ya\n");
//						flag1 = 1;
//						break;
//					}
//					else
//					{
//						c2++;
//					}
//				}
//			}
//			if (!flag1)
//				if (c2 >= 1)
//				{
//					printf("Da Jiang!!!\n");
//				}
//			flag1 = 0;
//			c2 = 0;
//		}
//
//	}
//}
//#include<bits/stdc++.h>
//using namespace std;
//int main()
//{
//	int a, b;
//	cin >> a >> b;
//	cout << a + b - 16 << endl;
//	cout << a + b - 3 << endl;
//	cout << a + b - 1 << endl;
//	cout << a + b << endl;
//}
//v#include<bits/stdc++.h>
//using namespace std;
//int main()
//{
//	int a, b;
//	cin >> a >> b;
//	cout << a + b - 16 << endl;
//	cout << a + b - 3 << endl;
//	cout << a + b - 1 << endl;
//	cout << a + b << endl;
//}
//#include<bits/stdc++.h>
//using namespace std;
//
//int main()
//{
//	cout << "Good code is its own best documentation.";
//	return 0;
//}
//

//#include <stdio.h>
//#include <string.h>
//
//int main() {
//    char adm[100], code[100];
//    while (scanf("%s %s", adm, code) != EOF)
//    {
//        if (strcmp("admin", adm) && strcmp("admin", code))
//            printf("Login Success!\n");
//        else
//            printf("Login Fail!\n");
//        getchar();
//    }
//}

//#include <stdio.h>
//
//int main()
//{
//    int m, n, count = 0;
//    scanf("%d %d", &m, &n);
//    int arr1[101][101], arr2[101][101];
//    for (int i = 0; i < m; i++)
//        for (int j = 0; j < n; j++)
//            scanf("%d", &arr1[i][j]);
//    for (int i = 0; i < m; i++)
//        for (int j = 0; j < n; j++)
//            scanf("%d", &arr2[i][j]);
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            if (arr1[i][j] == arr2[i][j])
//                count++;
//        }
//    }
//    printf("%.2f", count / (float)m / n * 100);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int num, arr[101],count=0;
//    scanf("%d", &num);
//    for (int i = 2; i <= num; i++)
//        arr[i - 2] = i;
//    for (int j = 0; j <= num - 2; j++)
//    {
//        for (int i = 2; i < arr[j]; i++)
//        {
//            if (arr[j] % i == 0)
//            {
//                arr[j] = 0;
//                count++;
//            }
//                
//        }
//    }
//    for (int i = 0; i <= num - 2; i++)
//    {
//        if (arr[i] != 0)
//            printf("%d ", arr[i]);
//    }
//    printf("\n%d\n", count);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int arr[51] = { 0 };
//    int m = 0;
//    scanf("%d", &m);
//    for (int i = 0; i < m; i++)
//        scanf("%d", &arr[i]);  //input
//    int ins = 0;
//    scanf("%d", &ins);
//    for (int j = m - 1; j >= 0; j--)
//    {
//        if (arr[j] > ins)
//        {
//            arr[j + 1] = arr[j];
//            arr[j] = ins;
//        }
//        else
//        {
//            arr[j + 1] = ins;
//            break;
//        }
//    }
//    for (int t = 0; t < m + 1; t++)
//        printf("%d ", arr[t]);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int m, arr[51] = { 0 };
//    scanf("%d", &m);
//    for (int i = 0; i < m; i++)
//        scanf("%d", &arr[i]);
//    int q;
//    scanf("%d", &q);
//    int pos = 0;
//    for (int j = 0; j <= m; j++)
//    {
//        if (arr[j] > q)
//        {
//            pos = j;
//            break;
//        }
//        if (q > arr[m - 1])
//        {
//            pos = m;
//            break;
//        }
//    }
//    for (int n = m; n > pos; n--)
//    {
//        arr[n] = arr[n - 1];
//    }
//    arr[pos] = q;
//    for (int out = 0; out <= m; out++)
//        printf("%d ", arr[out]);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    //多组输入
//    while (~scanf("%d", &n))
//    {
//        //控制行数
//        for (int i = 1; i <= n; i++)
//        {
//            //打印一行
//            for (int j = 1; j <= i; j++)
//            {
//                printf("%d ", j);
//            }
//            printf("\n");
//        }
//    }
//
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a, max = 0, small = 100, sum = 0, count = 0;
//	while (scanf("%d", &a) != EOF)
//	{
//		if (a > max)//判定最高分
//		{
//			max = a;
//		}
//		if (a < small)//判定最低分
//		{
//			small = a;
//		}
//		sum += a;
//		count++;//计数器
//		if (count == 7)//计数器=7时代表一组的分数好了可以进行计算
//		{
//			printf("%.2f\n", (sum - max - small) / 5.0);
//			count = 0;//重置
//			max = 0;//重置
//			small = 100;//重置
//			sum = 0;//重置
//		}
//
//	}
//
//	return 0;
//}

//BC68
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的X形图案。
//输入描述：
//多组输入，一个整数（2~20），表示输出的行数，也表示组成“X”的反斜线和正斜线的长度。
//输出描述：
//针对每行输入，输出用“ * ”组成的X形图案。
////找到规律是关键，看作一条正斜杠和反斜杠
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)  //外循环为行
//        {
//            for (int j = 0; j < n; j++) //内循环为列
//            {
//                if (i == j || i + j == n - 1)
//                    //最关键的地方，正斜线为[i][i]处是*， 反斜杠为[i][n-1-j]处是*，一行打印1个或2个*
//                    printf("*");
//                else
//                    printf(" ");
//            }
//            printf("\n"); //打印完一行，换行
//        }
//    }
//    return 0;
//}

//BC67
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的正斜线形图案。
//输入描述：
//多组输入，一个整数（2~20），表示输出的行数，也表示组成正斜线的“ * ”的数量。
//输出描述：
//针对每行输入，输出用“ * ”组成的正斜线。
//#include<stdio.h>
//int main()
//{
//    int i, j, n;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (i = n; i > 0; i--)
//        {
//            for (j = i - 1; j > 0; j--)
//                printf(" ");
//            printf("*\n");
//        }
//    }
//}


//BC66
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的反斜线形图案。
//输入描述：
//多组输入，一个整数（2~20），表示输出的行数，也表示组成反斜线的“ * ”的数量。
//
//输出描述：
//针对每行输入，输出用“ * ”组成的反斜线。
//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    //多组输入
//    while (~scanf(" %d", &n))
//    {
//        //控制行数
//        for (int i = 0; i < n; i++)
//        {
//            //控制列
//            for (int j = 0; j < n; j++)
//            {
//                //控制打印
//                if (i == j)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//            }
//            printf("\n");
//        }
//    }
//
//    return 0;
//}

//BC65
//
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的箭形图案。
//输入描述：
//本题多组输入，每行一个整数（2~20）。
//输出描述：
//针对每行输入，输出用“ * ”组成的箭形。
//#include<stdio.h>
//int main() {
//    int num;
//    int i, j, k;
//    while (scanf("%d", &num) != EOF)
//        //将火箭图案从中间分开 上部分一个大循环 下部分一个循环
//    {
//        for (i = 0; i <= num;
//            i++) { //上部分循环从此开始  此处确定了上部分循环的行数
//            for (j = 0; j < num - i; j++) { //此处为先开始打印空格
//                printf("  ");
//            }
//            for (k = 0; k <= i; k++) { //此处打印图案*
//                printf("*");
//            }
//            printf("\n");//至此一次大循环完成 打印了一行的图形 在此换行
//        }
//        for (i = 0; i < num; i++) {
//            for (j = 0; j <= i; j++) {
//                printf("  ");
//            }
//            for (k = 0; k < num - i; k++) {
//                printf("*");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

//BC64
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的K形图案。
//输入描述：
//多组输入，一个整数（2~20）。
//
//输出描述：
//针对每行输入，输出用“ * ”组成的K形，每个“ * ”后面有一个空格。
//
//#include <stdio.h>
//int main() {
//    int n = 0;
//    while (scanf("%d", &n) != EOF) {
//        //上一半
//        for (int i = n + 1; i > 0; i--) {
//            for (int j = i; j > 0; j--) {
//                printf("* ");
//            }
//            printf("\n");
//        }
//        //下一半
//        for (int i = 1; i <= n; i++) {
//            for (int j = 0; j <= i; j++) {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//

//BC63
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的菱形图案。
//输入描述：
//多组输入，一个整数（2~20）。
//
//输出描述：
//针对每行输入，输出用“ * ”组成的菱形，每个“ * ”后面有一个空格
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        //将菱形分成两部分 上部是n+1行打印 下部是n行打印
//        for (int i = 0; i < n + 1; i++)
//        {
//            for (int j = 0; j < n - i; j++)
//            {
//                printf(" ");
//            }
//            for (int j = 0; j <= i; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j <= i; j++)
//            {
//                printf(" ");
//            }
//            for (int j = 0; j < n - i; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//
//
//    return 0;
//}

//BC62
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的翻转金字塔图案。
//输入描述：
//多组输入，一个整数（2~20），表示翻转金字塔边的长度，即“ * ”的数量，也表示输出行数。
//输出描述：
//针对每行输入，输出用“ * ”组成的金字塔，每个“ * ”后面有一个空格。
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    while (scanf("%d", &a) != EOF)
//    {
//        for (int i = a; i > 0; i--)
//        {
//            for (int x = a - i; x > 0; x--)
//                printf(" ");
//            for (int j = i; j > 0; j--)
//                printf("* ");
//            printf("\n");
//        }
//    }
//    return 0;
//}

//BC61
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的金字塔图案。
//输入描述：
//多组输入，一个整数（2~20），表示金字塔边的长度，即“ * ”的数量，，也表示输出行数。
//输出描述：
//针对每行输入，输出用“ * ”组成的金字塔，每个“ * ”后面有一个空格。
//#include <stdio.h>
//
//int main()
//{
//    int n;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            for (int i = n - 1; i > j; i--)
//                printf(" ");
//            for (int k = 0; k <= j; k++)
//            {
//                printf("*");
//                if(k<j)
//                printf(" ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

//BC60
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的带空格直角三角形图案。
//
//输入描述：
//多组输入，一个整数（2~20），表示直角三角形直角边的长度，即“ * ”的数量，也表示输出行数。
//
//输出描述：
//针对每行输入，输出用“ * ”组成的对应长度的直角三角形，每个“ * ”后面有一个空格。
//#include<stdio.h>
//int main() {
//	int a;
//	while (scanf("%d", &a) != EOF) {
//		int i, k;
//		for (k = 0; k < a; k++) {
//			for (i = 0; i < (a - k - 1); i++) {
//				printf("  ");
//			}
//			for (i = 0; i <= k; i++) {
//				printf("* ");
//			}
//			printf("\n");
//		}
//	}
//	return 0;
//}

//BC59
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的翻转直角三角形图案。
//输入描述：
//多组输入，一个整数（2~20），表示翻转直角三角形直角边的长度，即“ * ”的数量，也表示输出行数。
//输出描述：
//针对每行输入，输出用“ * ”组成的对应长度的翻转直角三角形，每个“ * ”后面有一个空格。
//#include<stdio.h>
//int main() {
//	int a;
//	while (scanf("%d", &a) != EOF) {
//		int i, k;
//		for (k = a; k > 0; k--) {
//			for (i = k; i > 0; i--) {
//				printf("* ");
//			}
//			printf("\n");
//		}
//	}
//	return 0;
//}

//BC58
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的直角三角形图案。
//输入描述：
//多组输入，一个整数（2~20），表示直角三角形直角边的长度，即“ * ”的数量，也表示输出行数。
//输出描述：
//针对每行输入，输出用“ * ”组成的对应长度的直角三角形，每个“ * ”后面有一个空格
//#include<stdio.h>
//
//int main()
//{
//    int n;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int a = 0; a <= i; a++)
//                printf("* ");
//            printf("\n");
//        }
//    }
//    return 0;
//}

//BC57
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的正方形图案。
//输入描述：
//多组输入，一个整数（1~20），表示正方形的长度，也表示输出行数。
//
//输出描述：
//针对每行输入，输出用“ * ”组成的对应边长的正方形，每个“ * ”后面有一个空格
//#include<stdio.h>
//int main() {
//    int x;
//    while (scanf("%d", &x) != EOF) {
//        for (int i = 1; i <= x; i++) {
//            for (int j = 1; j <= x; j++) {
//                if (j == x) printf("*\n");
//                else printf("* ");
//            }
//        }
//    }
//}

//BC56
//描述
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的线段图案。
//
//输入描述：
//多组输入，一个整数（1~100），表示线段长度，即“ * ”的数量。
//输出描述：
//针对每行输入，输出占一行，用“ * ”组成的对应长度的线段。
//#include <stdio.h>
//
//int main()
//{
//    int n;
//    while (scanf("%d", &n) != -1)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            printf("*");
//        }
//        printf("\n");
//    }
//    return 0;
//}
//

//BC55
//描述
//KiKi实现一个简单计算器，实现两个数的“加减乘除”运算，用户从键盘输入算式“操作数1运算符操作数2”，计算并输出表达式的值，如果输入的运算符号不包括在（ + 、 - 、 * 、 / ）范围内，输出“Invalid operation!”。当运算符为除法运算，即“ / ”时。如果操作数2等于0.0，则输出“Wrong!Division by zero!”
//
//数据范围：字符串长度满足
//3
//≤
//�
//≤
//50
//
//3≤n≤50  ，保证运算符是一个char类型字符。
//输入描述：
//输入一行字符串，操作数1 + 运算符 + 操作数2 （其中合法的运算符包括： + 、 - 、 * 、 / ）。
//输出描述：
//输出为一行。
//
//如果操作数和运算符号均合法，则输出一个表达式，操作数1运算符操作数2 = 运算结果，各数小数点后均保留4位，数和符号之间没有空格。
//
//如果输入的运算符号不包括在（ + 、 - 、 * 、 / ）范围内，输出“Invalid operation!”。当运算符为除法运算，即“ / ”时。
//
//如果操作数2等于0.0，则输出“Wrong!Division by zero!”。
//#include<stdio.h>
//int main()
//{
//    double a, b;
//    char ch;
//    while (scanf("%lf %c %lf", &a, &ch, &b) != EOF)
//    {
//        if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
//        {
//            if (ch == '+')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a + b);
//            else if (ch == '-')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a - b);
//            else if (ch == '*')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a * b);
//            else
//            {
//                if (b == 0.0)
//                    printf("Wrong!Division by zero!\n");
//                else
//                    printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a / b);
//            }
//        }
//        else
//            printf("Invalid operation!\n");
//    }
//    return 0;
//}
//

//BC54
//描述
//KiKi想获得某年某月有多少天，请帮他编程实现。输入年份和月份，计算这一年这个月有多少天。
//输入描述：
//多组输入，一行有两个整数，分别表示年份和月份，用空格分隔。
//输出描述：
//针对每组输入，输出为一行，一个整数，表示这一年这个月有多少天。
//#include <stdio.h>
//
//int main()
//{
//    int a, b;
//    int leap[12] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
//    int not_leap[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//    while (scanf("%d %d", &a, &b) != EOF)
//    {
//        if ((a % 4 == 0 && a % 100 != 0) || (a % 400 == 0))
//        {
//            printf("%d\n", leap[b - 1]);
//        }
//        else
//            printf("%d\n", not_leap[b - 1]);
//    }
//    return 0;
//}

//BC50
//描述
//KiKi最近学习了信号与系统课程，这门课里有一个非常有趣的函数，单位阶跃函数，其中一种定义方式为：
//
//
//
//现在试求单位冲激函数在时域t上的值。
//
//输入描述：
//题目有多组输入数据，每一行输入一个t(-1000
//	输出描述：
//	输出函数的值并换行。
//#include <stdio.h>
//
//    int main()
//{
//    int n;
//    while (scanf("%d", &n) != EOF)
//    {
//        if (n > 0)
//            printf("1\n");
//        else if (n == 0)
//            printf("0.5\n");
//        else
//            printf("0\n");
//    }
//    return 0;
//}

//BC49
//描述
//KiKi想知道从键盘输入的两个数的大小关系，请编程实现。
//输入描述：
//题目有多组输入数据，每一行输入两个整数（范围 - 231~231 - 1），用空格分隔。
//输出描述：
//针对每行输入，输出两个整数及其大小关系，数字和关系运算符之间没有空格，详见输入输出样例
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF)
//    {
//        if (a > b)
//            printf("%d>%d\n", a, b);
//        else if (a < b)
//            printf("%d<%d\n", a, b);
//        else
//            printf("%d=%d\n", a, b);
//        getchar();
//    }
//    return 0;
//}

//BC48
//描述
//KiKi想完成字母大小写转换，有一个字符，判断它是否为大写字母，如果是，将它转换成小写字母；反之则转换为大写字母。
//输入描述：
//多组输入，每一行输入一个字母。
//输出描述：
//针对每组输入，输出单独占一行，输出字母的对应形式。
//#include <stdio.h>
//
//int main() {
//    char s;
//    while (scanf("%c", &s) != EOF)
//    {
//        if (s >= 'a' && s <= 'z')
//            s = s - 32;
//        else
//            s = s + 32;
//        printf("%c\n", s);
//        getchar();
//    }
//
//    return 0;
//}


//BC46
//描述
//KiKi开始学习英文字母，BoBo老师告诉他，有五个字母A(a), E(e), I(i), O(o), U(u)称为元音，其他所有字母称为辅音，请帮他编写程序判断输入的字母是元音（Vowel）还是辅音（Consonant）。
//输入描述：
//多组输入，每行输入一个字母。
//输出描述：
//针对每组输入，输出为一行，如果输入字母是元音（包括大小写），输出“Vowel”，如果输入字母是非元音，输出“Consonant”。
//
//#include <stdio.h>
//
//int main()
//{
//    char s;
//    while (scanf("%c", &s) != EOF)
//    {
//        if (s >= 'a' && s <= 'z')
//            s = s - 32;
//        if (s == 'A' || s == 'E' || s == 'I' || s == 'O' || s == 'U')
//            printf("Vowel\n");
//        else
//            printf("Consonant\n");
//        getchar();
//    }
//    return 0;
//}

//BC45
//描述
//KiKi参加了语文、数学、外语的考试，请帮他判断三科中的最高分。从键盘任意输入三个整数表示的分数，编程判断其中的最高分。
//
//数据范围：
//0
//≤
//�
//≤
//100
//
//0≤n≤100
//输入描述：
//输入一行包括三个整数表示的分数（0~100），用空格分隔。
//输出描述：
//输出为一行，即三个分数中的最高分。
//#include <stdio.h>
//
//int main()
//{
//    int a, b, c;
//    scanf("%d %d %d", &a, &b, &c);
//    if (b > a)
//        a = b;
//    if (c > a)
//        a = c;
//    printf("%d", a);
//    return 0;
//}

//BC44
//描述
//KiKi想知道一个整数的奇偶性，请帮他判断。从键盘任意输入一个整数（范围 - 231~231 - 1），编程判断它的奇偶性。
//输入描述：
//多组输入，每行输入包括一个整数。
//输出描述：
//针对每行输入，输出该数是奇数（Odd）还是偶数（Even）。
//#include <stdio.h>
//
//int main() {
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        if (a % 2 == 0)
//            printf("Even\n");
//        else
//            printf("Odd\n");
//    }
//    return 0;
//}

//BC43
//描述
//KiKi想知道他的考试分数是否通过，请帮他判断。从键盘任意输入一个整数表示的分数，编程判断该分数是否在及格范围内，如果及格，即：分数大于等于60分，是输出“Pass”，否则，输出“Fail”。
//输入描述：
//多组输入，每行输入包括一个整数表示的分数（0~100）。
//输出描述：
//针对每行输入，输出“Pass”或“Fail”
//
//#include <stdio.h>
//
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        if (a >= 60)
//            printf("Pass\n");
//        else
//            printf("Fail\n");
//    }
//    return 0;
//}
//
//
//BC42
//描述
//KiKi想知道他的考试成绩是否完美，请帮他判断。从键盘输入一个整数表示的成绩，编程判断成绩是否在90~100之间，如果是则输出“Perfect”。
//
//输入描述：
//多组输入，每行输入包括一个整数表示的成绩（90~100）。
//输出描述：
//针对每行输入，输出“Perfect”。
//
//#include <stdio.h>
//
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        if (a >= 90 && a <= 100)
//            printf("Perfect\n");
//    }
//    return 0;
//}

//描述
//假设你们社团要竞选社长，有两名候选人分别是A和B，社团每名同学必须并且只能投一票，最终得票多的人为社长.
//输入描述：
//一行，字符序列，包含A或B，输入以字符0结束。
//输出描述：
//一行，一个字符，A或B或E，输出A表示A得票数多，输出B表示B得票数多，输出E表示二人得票数相等。
//
//#include <stdio.h>
//
//int main()
//{
//    char arr[100];
//    scanf("%s", arr);
//    int a = 0, b = 0, c = 0;
//    while (arr[c] != '0')
//    {
//        if (arr[c] == 'A')
//            a++;
//        else
//            b++;
//        c++;
//    }
//    if (a == b)
//        printf("E\n");
//    else if (a > b)
//        printf("A\n");
//    else
//        printf("B\n");
//    return 0;
//}

//#include<stdio.h>
//char a[10][10];
//int i, j, n;
//void f1()
//{
//	printf("二元关系的域的个数：\n");
//	scanf("%d", &n);
//	printf("输入关系矩阵\n");
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			scanf("%d", &a[i][j]);
//		}
//	}
//}
//
//int f2()
//{
//	for (int i = 0; i < n; i++)
//	{
//		if (a[i][j] == 1)
//		{
//			printf("具有自反性\n");
//			return 1;
//		}
//		else if (a[i][j] != 1)
//		{
//			printf("不具有自反性\n");
//			return 0;
//		}
//	}
//	return 1;
//}
//
//int f3()
//{
//	for (int i = 0; i < n; ++i)
//	{
//		for (int j = 1; j < n; ++j)
//		{
//			if (a[i][j] == a[j][i])
//			{
//				printf("具有对称性\n");
//				return 1;
//			}
//			else if (a[i][j] != a[j][i])
//			{
//				printf("不具有对称性\n");
//				return 0;
//			}
//			break;
//		}
//	}
//	return 1;
//}
//
//int f4()
//{
//	for (int i = 0; i < n; ++i)
//	{
//		for (int j = 0; j < n; ++j)
//		{
//			for (int k = 0; k < n; ++k)
//			{
//				if (a[i][j] && a[j][k] && !a[i][k])
//				{
//					printf("不具有传递性\n");
//					return 0;
//				}
//				else
//				{
//					printf("具有传递性\n");
//					return 1;
//				}
//			}
//		}
//	}
//	return 1;
//}
//
//int main()
//{
//	f1();
//	if (f2() && f3() && f4())
//	{
//		printf("具有等价关系\n");
//	}
//	else
//	{
//		printf("不具有等价关系\n");
//	}
//	return 0;
//}
//
//#include<stdio.h>
//void jiao(int a[30], int b[30], int c[30], int p, int q)
//{
//    int i, j, k = 0;
//    for (i = 0; i < p; i++)
//    {
//        for (j = 0; j < q; j++)
//        {
//            if (a[i] == b[j])
//            {
//                c[k] = a[i];
//                k++;
//            }
//        }
//    }
//    printf("交集：{");
//    for (i = 0; i < k; i++)
//    {
//        if (i == k - 1)
//        {
//            printf("%d", c[i]);
//        }
//        else printf("%d,", c[i]);
//    }
//    printf("}\n");
//}
//
//void bing(int a[30], int b[30], int c[30], int p, int q)
//{
//    int i, j, n = 0, k = 0;
//    for (i = 0; i < p; i++)
//    {
//        c[i] = a[i];
//    }
//    for (j = 0; j < q; j++)
//    {
//        c[i] = b[j];
//        i++;
//    }
//    for (i = 0; i < p; i++)
//    {
//        for (j = 0; j < q; j++)
//        {
//            if (a[i] == b[j])
//            {
//                n++;
//            }
//        }
//    }
//    for (i = 0; i < p + q; i++)
//    {
//        for (j = 0; j < p + q; j++)
//        {
//            if (c[i] == c[j] && i != j)
//            {
//                for (k = j; k < p + q; k++)
//                {
//                    c[k] = c[k + 1];
//                }
//            }
//        }
//    }
//    printf("并集：{");
//    for (i = 0; i < k - n; i++)
//    {
//        if (i == k - n - 1)
//        {
//            printf("%d", c[i]);
//        }
//        else printf("%d,", c[i]);
//    }
//    if (n == 0)
//    {
//        for (i = 0; i < p + q; i++)
//        {
//            if (i == p + q - 1)
//            {
//                printf("%d", c[i]);
//            }
//            else printf("%d,", c[i]);
//        }
//    }
//    printf("}\n");
//}
//
//void cha(int a[30], int b[30], int c[30], int p, int q)
//{
//    int i, j, k, m = 0;
//    for (i = 0; i < p; i++)
//    {
//        for (j = 0; j < q; j++)
//        {
//            k = 0;
//            if (a[i] == b[j])
//            {
//                k = 1;
//            }
//            if (k)
//                break;
//        }
//        if (k == 0)
//        {
//            c[m] = a[i];
//            m++;
//        }
//    }
//    printf("差集{");
//    for (i = 0; i < m; i++)
//    {
//        if (i != m - 1)
//            printf("%d,", c[i]);
//        else
//            printf("%d", c[i]);
//    }
//    printf("}\n");
//}
//
//void bu(int a[30], int e[30], int c[30], int p, int q)
//{
//    int i, j, n = 0, m = 2;
//    for (i = 0; i < q; i++)
//    {
//        for (j = 0; j < p; j++)
//        {
//            if (e[i] == a[j])
//            {
//                break;
//            }
//            if (e[i] != a[j] && j == p - 1)
//            {
//                c[n] = e[i];
//                n++;
//            }
//        }
//    }
//    printf("补集：{");
//    for (i = 0; i < n; i++)
//    {
//        if (i != n - 1)
//            printf("%d,", c[i]);
//        else
//            printf("%d", c[i]);
//    }
//    printf("}\n");
//}
//
//int main()
//{
//    int n1, n2, i, j, t, s, n, k, m;
//    int a[200], b[200], c[200], d[200], e[200];
//    printf("集合A元素个数");
//    scanf("%d", &n1);
//    for (i = 0, j = 0; i < n1; i++, j++) 
//    {
//        scanf("%d", &a[i]);
//        d[j] = a[i];
//        for (n = 0; n < i; n++) 
//        {
//            if (a[i] == a[n]) 
//            {
//                printf("重新输入");
//                for (i = 0, j = 0; i < n1; i++, j++) 
//                {
//                    scanf("%d", &a[i]);
//                    d[j] = a[i];
//                }
//            }
//            break;
//        }
//    }
//    printf("集合B元素个数");
//    scanf("%d", &n2);
//    for (i = 0, j = n1; i < n2; i++, j++) 
//    {
//        scanf("%d", &b[i]);
//        d[j] = b[i];
//        for (k = 0; k < i; k++) 
//        {
//            if (b[k] == b[i]) 
//            {
//                printf("重新输入");
//                for (i = 0, j = n1; i < n2; i++, j++) 
//                {
//                    scanf("%d", &b[i]);
//                    d[j] = b[i];
//                }
//                break;
//            }
//        }
//
//    }
//    printf("集合E元素个数");
//    scanf("%d", &m);
//    for (i = 0; i < m; i++) 
//    {
//        scanf("%d", &e[i]);
//        for (k = 0; k < i; k++) 
//        {
//            if (e[k] == e[i]) 
//            {
//                printf("重新输入");
//                for (i = 0; i < m; i++) 
//                {
//                    scanf("%d", &e[i]);
//                }
//                break;
//            }
//        }
//    }
//    jiao(a, b, c, n1, n2);
//    bing(a, b, c, n1, n2);
//    cha(a, b, c, n1, n2);
//    bu(a, e, c, n1, m);
//}

//BC39
//描述
//期中考试开始了，大家都想取得好成绩，争夺前五名。从键盘输入 n 个学生成绩，输出每组排在前五高的成绩。
//5≤n≤50  ，成绩采取百分制并不会出现负数
//输入描述：
//两行，第一行输入一个整数，表示n个学生（ >= 5），第二行输入n个学生成绩（整数表示，范围0~100），用空格分隔。
//输出描述：
//一行，输出成绩最高的前五个，用空格分隔。
//#include<stdio.h>
//int main() {
//    int arr[40] = { 0 };
//    int num = 0;
//    scanf("%d", &num);//输入几名
//    for (int k = 0; k <= num; k++) {//输入成绩
//        scanf("%d", &arr[k]);
//    }
//    int temp = 0;
//    for (int i = 0; i < num; i++) {//第i位为最大数
//        for (int j = i + 1; j < num; j++) {//从第i+1位开始遍历剩余数
//            if (arr[j] > arr[i]) {//存在大于最大数的数
//                temp = arr[i];//把最大数和比较数进行交换
//                arr[i] = arr[j];
//                arr[j] = temp;
//            }
//
//        }
//    }
//    for (int k = 0; k < 5; k++)
//        printf("%d ", arr[k]);
//
//    return 0;
//}

//BC38
//描述
//变种水仙花数 - Lily Number：把任意的数字，从中间拆分成两个数字，比如1461 可以拆分成（1和461）, （14和61）, （146和1), 如果所有拆分后的乘积之和等于自身，则是一个Lily Number。
//
//例如：
//
//655 = 6 * 55 + 65 * 5
//
//1461 = 1 * 461 + 14 * 61 + 146 * 1
//
//求出 5位数中的所有 Lily Number。
//
//输入描述：
//无
//输出描述：
//一行，5位数中的所有 Lily Number，每两个数之间间隔一个空格。
//#include <stdio.h>
//int main()
//{
//    for (int i = 10000; i <= 99999; i++)
//    {
//        if (((i/10000)*(i%10000)+(i / 1000) * (i % 1000) + (i / 100) * (i % 100) + (i / 10) * (i % 10) ) == i)
//            printf("%d ", i);
//    }
//    printf("\b");
//    return 0;
//}