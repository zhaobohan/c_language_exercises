#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int n, a[100][100], m = 0, visit[100];
int cur = 0, s[100], vis[100][100];

void try1(int k)//取欧拉路
{
    int i;
    if (cur == m + 1)
    {
        for (i = 0; i < cur; i++)
        {
            if (i == 0)
                printf("%d", s[i] + 1);
            else
                printf("->%d", s[i] + 1);
        }
        printf("\n");
    }
    else
    {
        for (i = 0; i < n; i++)
        {
            if (a[k][i] && !vis[k][i])
            {
                vis[k][i] = 1;
                s[cur++] = i;
                try1(i);
                cur--;
                vis[k][i] = 0;
            }
        }
    }
}

void dfs(int k)
{
    int i;
    visit[k] = 1;
    for (i = 0; i < n; i++)
    {
        if (a[k][i] && !visit[i])
        {
            dfs(i);
        }
    }
}

int fun()//判断连通性
{
    int i, j;
    for (i = 0; i < n; i++)
    {
        memset(visit, 0, sizeof(visit));
        dfs(i);
        for (j = 0; j < n; j++)
        {
            if (!visit[j])
                break;
        }
        if (j == n)
            return 1;
    }
    return 0;
}

int main()
{
    int i, j;
    printf("请输入节点个数n：");
    scanf("%d", &n);

    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            scanf("%d", &a[i][j]);

    if (fun() == 0)
    {
        printf("该图不是连通图！\n");
        return 0;
    }

    printf("该图是连通图！\n");

    int odd = 0, begin = -1, end = -1;

    for (i = 0; i < n; i++)
    {
        int sum1 = 0, sum2 = 0;
        for (j = 0; j < n; j++)
        {
            sum1 += a[i][j];
            sum2 += a[j][i];
        }
        if (sum1 != sum2)
        {
            odd++;
            if (sum1 - sum2 == 1)
                begin = i;
            if (sum2 - sum1 == 1)
                end = i;
        }
    }

    if (odd == 0)
    {
        printf("该图是欧拉图。\n");
        printf("所有的欧拉路为：\n");
        for (i = 0; i < n; i++)
        {
            s[cur++] = i;
            try1(i);
            cur--;
        }
    }

    else
       printf("不是欧拉图,也不是半欧拉图\n");

    return 0;
}

//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
//#include <time.h>
//
//int n, a[100][100], m = 0, visit[100];
//int cur = 0, s[100], vis[100][100];
//
//void try1(int k)//取欧拉路
//{
//    int i;
//    if (cur == m + 1)
//    {
//        for (i = 0; i < cur; i++)
//        {
//            if (i == 0)
//                printf("%d", s[i] + 1);
//            else
//                printf("->%d", s[i] + 1);
//        }
//        printf("\n");
//    }
//    else
//    {
//        for (i = 0; i < n; i++)
//        {
//            if (a[k][i] && !vis[k][i])
//            {
//                vis[k][i] = 1;
//                vis[i][k] = 1;
//                s[cur++] = i;
//                try1(i);
//                cur--;
//                vis[k][i] = 0;
//                vis[i][k] = 0;
//            }
//        }
//    }
//}
//
//void dfs(int k)
//{
//    int i;
//    visit[k] = 1;
//    for (i = 0; i < n; i++)
//    {
//        if (a[k][i] && !visit[i])
//        {
//            dfs(i);
//        }
//    }
//}
//
//int fun()//判断连通性
//{
//    int i;
//    dfs(0);
//    for (i = 0; i < n; i++)
//    {
//        if (!visit[i])
//            return 0;
//    }
//    return 1;
//}
//
//int main()
//{
//    int i, j;
//    printf("请输入节点个数n：");
//    scanf("%d", &n);
//    for (i = 0; i < n; i++)
//        for (j = 0; j < n; j++)
//            scanf("%d", &a[i][j]);
//    printf("邻接矩阵为：\n");
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n; j++)
//            printf("%d ", a[i][j]);
//        printf("\n");
//    }
//    if (fun() == 0)
//    {
//        printf("该图不是连通图\n");
//        return 0;
//    }
//    printf("该图是连通图\n");
//    int odd = 0;
//    for (i = 0; i < n; i++)
//    {
//        int sum = 0;
//        for (j = 0; j < n; j++)
//        {
//            if (a[i][j])
//                sum++;
//        }
//        if (sum % 2)
//            odd++;
//    }
//
//    if (odd == 0)
//    {
//        printf("该图是欧拉图。\n");
//        printf("所有的欧拉路为：\n");
//        for (i = 0; i < n; i++)
//        {
//            s[cur++] = i;
//            try1(i);
//            cur--;
//        }
//    }
//    else
//       printf("不是欧拉图\n");
//    return 0;
//}